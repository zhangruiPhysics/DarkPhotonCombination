v0
 - dummy input to test the comb framework
v1
 - version with paper results
v2: 
 - version with a different luminosity NP implementation and request prefit version instead of postfit so that all NP init values are zero
 - test version for VBF from Khan (taken from https://indico.cern.ch/event/1301071/contributions/5470658/attachments/2675301/4639150/Intro_Jun28.pdf)
