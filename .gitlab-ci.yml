stages:
  - build
  - process
  - combine
  - plotting
  - download

image: atlasadc/atlas-grid-centos7

Compile:
  stage: build
  tags:
    # Make you job be executed in a shared runner that has CVMFS mounted
    - cvmfs
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SSL_NO_VERIFY: "true"
  script:
    - set +e
    - shopt -s expand_aliases
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    - base_path=$(pwd)
    - git status
    - cd $base_path
    - source setup.sh compile
  artifacts:
    paths:
      - setup.sh
      - submodules
      - python

.IStep1:
  stage: process
  tags:
    - cvmfs
  needs:
    - job: Compile
  variables:
    GIT_SSL_NO_VERIFY: "true"
    input_dir: input
  before_script:
    - set +e
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - echo "lxplus.cern.ch, ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPZvfRF+9L7TR3FRPyLFdcsXSZ6RQYJHfOjzzWB94sX0gP34Cgij9p4ukL900sVVvw3LPM5OxxFSNIXGztFYu4o=" > ~/.ssh/known_hosts
    - echo -e "Host *\n\tGSSAPIDelegateCredentials yes\n\tGSSAPITrustDNS yes\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo $SERVICE_PASS | kinit -f $CERN_USER || echo something
    - mkdir -p $input_dir
    - rm -f $input_dir/$version
    - scp -r -i ~/.ssh/id_rsa ${CERN_USER}@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-exotics/cdm/Dark-photon-combination/workspaces/$version $input_dir/
    - ls -l $input_dir/$version/*/*
    - shopt -s expand_aliases
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    - source setup.sh || echo ignore setupATLAS return code
  artifacts:
    paths:
      - $output_dir

.IStep2:
  stage: combine
  tags:
    - cvmfs
  variables:
    GIT_SSL_NO_VERIFY: "true"
    input_dir: output_CI
  before_script:
    - set +e
    - shopt -s expand_aliases
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    - source setup.sh || echo ignore setupATLAS return code
  artifacts:
    paths:
      - $input_dir

###################################
## Process channels ##
###################################
.Process:
  extends: .IStep1
  tags:
    - cvmfs
  variables:
    output_dir: output_CI
    mini_option: default.json
    version: v3
    config_tag: v2
    other_options: "--tasks modification"
  allow_failure: true
  script:
    - echo Comb process_channels -i $input_dir/$version -c $channel -o $output_dir --minimizer_options config/minimizer/$mini_option --config config/task_config/darkphoton_${config_tag}.yaml --unblind $other_options
    - Comb process_channels -i $input_dir/$version -c $channel -o $output_dir --minimizer_options config/minimizer/$mini_option --config config/task_config/darkphoton_${config_tag}.yaml --unblind $other_options
    - for i in `\ls $output_dir/limits/*/*/limits*.json`; do echo $i; cat $i; done

VBF_ggF_ZH_pro:
  needs:
    - job: Compile
  tags:
    - cvmfs
  extends: .Process
  variables:
    output_dir: output_CI
    channel: "VBFchannel,ZHchannel,ggFchannel"
  only:
    - branches
    - tags

VBF_ggF_ZH_pro_limit:
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_ggF_ZH_pro
      artifacts: true
  tags:
    - cvmfs
  extends: VBF_ggF_ZH_pro
  variables:
    other_options: "--tasks modification,limit"
  only:
    - tags
    - branches

VBF_subchan_pro:
  needs:
    - job: Compile
  tags:
    - cvmfs
  extends: .Process
  variables:
    output_dir: output_CI
    channel: VBFchannel_VBFsignalonly,VBFchannel_ggFsignalonly
  only:
    - branches
    - tags

VBF_subchan_pro_limit:
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_subchan_pro
      artifacts: true
  tags:
    - cvmfs
  extends: VBF_subchan_pro
  variables:
    other_options: "--tasks modification,limit"
  only:
    - tags
    - branches

###################################
## None-resonant combination     ##
###################################
.Combine_ws:
  extends: .IStep2
  tags:
    - cvmfs
  variables:
    mini_option: default.json
    np_option: correlation_v4.json
    task: combination,limit
    config_tag: v2
    other_options: ""
  allow_failure: true
  script:
    - echo Comb combine_channels -i $input_dir -c $channel --minimizer_options config/minimizer/$mini_option --tasks $task --config config/task_config/darkphoton_${config_tag}.yaml --unblind --scheme config/correlation/$np_option $other_options
    - Comb combine_channels -i $input_dir -c $channel --minimizer_options config/minimizer/$mini_option --tasks $task --config config/task_config/darkphoton_${config_tag}.yaml --unblind --scheme config/correlation/$np_option $other_options
    - echo Comb combine_channels -i $input_dir -c $channel --minimizer_options config/minimizer/$mini_option --tasks $task --config config/task_config/darkphoton_uncorr_${config_tag}.yaml --unblind $other_options
    - Comb combine_channels -i $input_dir -c $channel --minimizer_options config/minimizer/$mini_option --tasks $task --config config/task_config/darkphoton_uncorr_${config_tag}.yaml --unblind $other_options
    - for i in `\ls $input_dir/limit/combined/*/*/limits*.json`; do echo $i; cat $i; done

  only:
    - tags
    - branches

VBF_ZH_comb:
  needs:
    - job: Compile
    - job: VBF_ggF_ZH_pro
  extends: .Combine_ws
  allow_failure: false
  tags:
    - cvmfs
  variables:
    input_dir: output_CI
    channel: "VBFchannel,ZHchannel"
    other_options: "--filter mX=(125)"

VBF_ggF_comb:
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_ggF_ZH_pro
      artifacts: true
  extends: .Combine_ws
  allow_failure: false
  tags:
    - cvmfs
  variables:
    input_dir: output_CI
    channel: "VBFchannel,ggFchannel"
    other_options: "--exclude mX=(125)"

####################################
### Plotting                      ##
####################################
.plot:
  stage: plotting
  tags:
    - cvmfs
  variables:
    GIT_SSL_NO_VERIFY: "true"
  before_script:
    - set +e
    - shopt -s expand_aliases
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    - source setup.sh || echo ignore setupATLAS return code
  allow_failure: true
  only:
    - tags
    - branches

VBF_ggF_plot:
  extends: .plot
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_ggF_comb
      artifacts: true
    - job: VBF_ggF_ZH_pro_limit
      artifacts: true
  tags:
    - cvmfs
  variables:
    channel: "VBFchannel,ggFchannel"
    input_dir: output_CI
  script:
    - echo python notebook/limit_plotting.py --input_folder $input_dir --task BSM --channel $channel --combine_tags fullcorr
    - python notebook/limit_plotting.py --input_folder $input_dir --task BSM --channel $channel --combine_tags fullcorr
  artifacts:
    paths:
      - $input_dir/figures

VBF_ZH_plot:
  extends: .plot
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_ZH_comb
      artifacts: true
    - job: VBF_ggF_ZH_pro_limit
      artifacts: true
  tags:
    - cvmfs
  variables:
    channel: "VBFchannel,ZHchannel"
    input_dir: output_CI
  script:
    - echo python notebook/limit_plotting.py --input_folder $input_dir --task SM --channel $channel --combine_tags fullcorr
    - python notebook/limit_plotting.py --input_folder $input_dir --task SM --channel $channel --combine_tags fullcorr
  artifacts:
    paths:
      - $input_dir/figures

Correlation_plot:
  extends: .plot
  needs:
    - job: Compile
      artifacts: true
    - job: VBF_ZH_comb
      artifacts: true
    - job: VBF_ggF_ZH_pro_limit
      artifacts: true
    - job: VBF_ggF_comb
      artifacts: true
  tags:
    - cvmfs
  variables:
    threshold: "0.2"
    input_dir: output_CI
  script:
    - mkdir -p $input_dir/figures
    - for i in `\ls $input_dir/workspace/combined/*-fullcorr/*.root`; do echo quickstats np_correlation --threshold $threshold -i $i -f 'mu*channel=1,gamma*=1,*bin1=0,*PDF4LHC*=0' -r 'mu_SIG' -o $input_dir/figures/corr_`echo $i| cut -d'/' -f 4- | sed 's/\//_/g' | sed 's/.root$/.pdf/'`; done
    - for i in `\ls $input_dir/workspace/combined/*-fullcorr/*.root`; do quickstats np_correlation --threshold $threshold -i $i -f 'mu*channel=1,gamma*=1,*bin1=0,*PDF4LHC*=0' -r 'mu_SIG' -o $input_dir/figures/corr_`echo $i| cut -d'/' -f 4- | sed 's/\//_/g' | sed 's/.root$/.pdf/'`; done
  artifacts:
    paths:
      - $input_dir/figures


#####################################
## Intermediate output to download ##
#####################################
Artifacts:
  stage: download
  tags:
    - cvmfs
  variables:
    GIT_SSL_NO_VERIFY: "true"
    output: output_CI
  allow_failure: true
  needs:
    - job: VBF_ZH_plot
      artifacts: true
    - job: VBF_ggF_plot
      artifacts: true
    - job: Correlation_plot
      artifacts: true
    - job: VBF_subchan_pro_limit
      artifacts: true
    - job: VBF_ggF_ZH_pro_limit
      artifacts: true
    - job: VBF_ZH_comb
      artifacts: true
    - job: VBF_ggF_comb
      artifacts: true
  script:
    - ls *
  only:
    - tags
    - branches
  artifacts:
    paths:
      - $output
