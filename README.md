## Dark Photon Combination
This framework uses `quickstats` [gitlab](https://gitlab.cern.ch/clcheng/quickstats).
The environment is `LCG_103` which is shipped with `ROOT 6.28/00`.
To use a lower ROOT version, adjust `setup.sh`.
This framework does not use `asetup StatAnalysis,0.2.0` since some python packages such as `pandas` is missing.

**If you are a `quickFit` user, you can develop your `bash` scripts**

### Setup
For the first time, you need to compile the framework:
```
git clone --recursive https://:@gitlab.cern.ch:8443/zhangruiPhysics/DarkPhotonCombination.git
source setup.sh compile
```
If you have already cloned the repository and forgot to use --recursive, you can still update the submodules using the following command:
```
git submodule update --init --recursive
```

For the future time, just setup the environment by:
```
source setup.sh
```

### Run combination
We do two-step combination.

#### Process channels
The first step is to remake the input workspace by modifying some contents.
Possible and common things to do in this step are:
```
- Rename dataset, POI, NP, workspace, etc names
- Redefine POI(s) init value and range
- Rescale POI(s) to make sure different channels scale to the same unit
```
These actions can be defined in a YAML file.
More complicated are [example1](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_combination_fw/-/blob/master/configs/task_options/projection2022/proj_nonres_kl.yaml), [example2](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_combination_fw/-/blob/master/configs/task_options/CONF2021/nonres_v6_mH125p09.yaml), [example3](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_combination_fw/-/blob/master/configs/task_options/HHH2022/nonres_kl_kt_likelihood_with_BR_decorrelation_mH125p09.yaml).

Once the task option is defined, run the following command to reprocess the input workspaces:
```
Comb process_channels -i <input_folder> -o <output_folder> -c <channel_names> --minimizer_options <minimizer_config_json_file> --config <config_json_file_to_redefine_rescale_rename_poi_and_np> --do-limit
e.g. Comb process_channels -i input/v3 -c VBFchannel,ZHchannel,ggFchannel -o output_CI --minimizer_options config/minimizer/default.json --config config/task_config/darkphoton_v2.yaml --unblind --tasks modification,limit
e.g. Comb process_channels -i input/v3 -c VBFchannel_VBFsignalonly,VBFchannel_ggFsignalonly -o output_CI --minimizer_options config/minimizer/default.json --config config/task_config/darkphoton_v2.yaml --unblind --tasks modification,limit
```

After the jobs are finished, you expect to see the following output structure:
```
output/v2/
|-- limits                      # Folder where the limit results are stored
|   |-- ZH
|   |   |-- cache
|   |   |   |-- mass_125.json     # Limit results for each mass point for the ZH channel. Results will be summarised in limits.json file in the upper level folder
|   |   |   `-- mass_125.log
|   |   `-- limits.json         # Limit results for the ZH channel. Inside the file is a dictionay, "0" is the expected limit, "obs" is the observed limit, "+-1,+-2" are +-1,+-2 sigma band, respectively
|   `-- ggH
|       |-- cache
|       |   |-- mass_125.json
|       |   `-- mass_125.log
|       `-- limits.json
`-- workspace                   # Modified workspace. This file reflect the changes implemented in <config_json_file_to_redefine_rescale_rename_poi_and_np>. Will be used as the input for the next step
    |-- ZH
    |   |-- 0.log
    |   `-- 0.root
    `-- ggH
        |-- 0.log
        `-- 0.root
```

#### Combine channels
The second step is to combine workspaces.
In this step, you need to correlate nuisance parameters.
These are defined in a JSON file.
Omitting `-s/--scheme` will perform uncorrelated combination, i.e. no correlation at all.
```
Comb combine_channels -i <output_from_process_channels> -c <channel_name,channel_name> --minimizer_options <minimizer_config_json_file> --scheme <correlation_scheme_in_json_file> --unblind --tasks combination,limit
e.g. Comb combine_channels -i output_CI -c VBFchannel,ZHchannel --minimizer_options config/minimizer/default.json --tasks combination,limit --config config/task_config/darkphoton_v2.yaml --unblind --scheme config/correlation/correlation_v4.json --filter 'mX=(125)'
e.g. Comb combine_channels -i output_CI -c VBFchannel,ggFchannel --minimizer_options config/minimizer/default.json --tasks combination,limit --config config/task_config/darkphoton_v2.yaml --unblind --scheme config/correlation/correlation_v4.json --exclude 'mX=(125)'
```
The output folder structure now becomes:
```
output/v2/
|-- cfg
|   `-- combination
|       `-- ZH_ggH-nocorr
|           `-- <mass>.xml
|-- limits
|   |-- ZH
|   |   |-- cache
|   |   |   |-- mass_125.json
|   |   |   `-- mass_125.log
|   |   `-- limits.json
|   |-- combined
|   |   `-- ZH_ggH-nocorr
|   |       |-- cache
|   |       |   |-- mass_125.json
|   |       |   `-- mass_125.log
|   |       `-- limits.json     # Combined limit results
|   `-- ggH
|       |-- cache
|       |   |-- mass_125.json
|       |   `-- mass_125.log
|       `-- limits.json
|-- workspace
    |-- ZH
    |   |-- 0.log
    |   `-- 0.root
    |-- ggH
    |   |-- 0.log
    |   `-- 0.root
    |-- combined
       `-- ZH_ggH-nocorr
           |-- 0.log
           `-- 0.root              # Combined workspace
```

In both steps, can run other options:
```
--do-pvalue: calculate p-values/significance for POI=0
--do-likelihood: run likelihood scan
```

#### Generate correlation scheme file
Correlation scheme can be defined as a JSON file that has a certain pattern and can be used via `-s/--scheme` in the combination step.
Here is an [example](https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_combination_fw/-/blob/master/configs/correlation_schemes/CONF2021/spin0_v7.json).
It looks like:
```
{
    "channel":
    {
        "old_name": "new_name",
        ...
    }, ...  
}
```
This will rename the `old_name` in the rescaled workspace to `new_name`.
If two channels share the same `new_name`, this NP will be correlated in the combined workspace.
For example, if the luminosity NP is called `Lumi` in channel 1 and `LUMI` in channel 2, and we want to correlate them, we should write the scheme file like this:
```
{
   "channel1":
   {
        "lumi": "ATLAS_Lumi_Run2"
   },
   "channel2":
   {
        "LUMI": "ATLAS_Lumi_Run2"
   },
}
```
Besides implementing it by hand, one could use `quickstats` to help.
It should performs well in the ATLAS standard CP recommendations but still requires a lot of checks by eye.
For example, one can run the following command to generate the scheme file as `config/correlation/correlation_v1.json`.
```
quickstats harmonize_np -b input/v2/ -o config/correlation/correlation_v1.json -i config/correlation/input_list.json -r config/correlation/reference_list.json
```
It also prints out the renaming to screen for each channel and one should check carefully to make sure the renaming are intended.
One may also see something following at the end of each channel (not only in the very end of the print but in every channel block).
```
my_unique_np_name -> ?
```
This means the program does not get an instructure on how to rename the `my_unique_np_name` from `reference_list.json`.

If an uncertainty and its name are unique for this channel, namely if the original name can be kept, then you can leave with it (check the output JSON file to make sure the names are kept.

If you want to eliminate the question marks from the printout by explicitly renaming it rather leaving the original name, you should work on the reference file with something like the following (or other syntax for python regex).
```
"CUSTOMMAP": {
  "description": "NPs with custom mapping",
  "map": {
    "my_unique_np_name": "ATLAS_unique_np_name"
  }
}, ...
```

#### Plotting
To plot 1D limit plot:
```
python notebook/limit_plotting.py --input_folder output_CI --task SM --channel VBFchannel,ZHchannel --combine_tags fullcorr
```
To plot 2D limit band:
```
python notebook/limit_plotting.py --input_folder output_CI --task BSM --channel VBFchannel,ggFchannel --combine_tags fullcorr
```

#### Generate correlation matrix
```
quickstats np_correlation --threshold 0.2 -i output_CI/workspace/combined/VBFchannel_ZHchannel-fullcorr/125.root -o output_CI/figures/corr_VBFchannel_ZHchannel-fullcorr_125-2.pdf -f 'k*=1,gamma*=1' -r'mu_SIG=2_-1000_1000'
quickstats np_correlation --threshold 0.2 -i output_CI/workspace/combined/VBFchannel_ggFchannel-fullcorr/1000.root  -o output_CI/figures/corr_VBFchannel_ggFchannel-fullcorr_1000-2.pdf -f 'mu*channel=1,gamma*=1,*bin1=0,*PDF4LHC*=0' -r 'mu_SIG'
quickstats np_correlation --threshold 0.2 -i output_CI/workspace/combined/VBFchannel_ggFchannel-fullcorr/2000.root  -o output_CI/figures/corr_VBFchannel_ggFchannel-fullcorr_2000-2.pdf -f 'mu*channel=1,gamma*=1,*bin1=0,*PDF4LHC*=0' -r 'mu_SIG'
```
