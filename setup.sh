#!/usr/bin/env bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase # use your path
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# More memory
ulimit -S -s unlimited

export basedir=$PWD
export PATH=${basedir}/bin:${basedir}/scripts:${basedir}/submodules/quickstats/bin:$PATH
export PYTHONPATH=${basedir}/bin:${basedir}:${basedir}/submodules/quickstats:$PYTHONPATH

lcg=$1
PRETTY_NAME=`cat /etc/os-release | grep PRETTY_NAME`
if [[ $PRETTY_NAME =~ CentOS ]]; then
    gcc=x86_64-centos7-gcc11-opt
else
    gcc=x86_64-el9-gcc11-opt
    lcg=105
fi

if [[ -z $lcg ]] || [[ $lcg == 'compile' ]]; then # The default
    #echo 'setup LCG_103, ROOT 6.28/00'
    #lsetup "views LCG_103 $gcc"
    echo 'setup LCG_102b, ROOT 6.26/08'
    lsetup "views LCG_102b $gcc"
    if [[ $lcg == 'compile' ]]; then # First time to compile quickstats
        quickstats compile
    fi
elif [[ $lcg == '103' ]]; then
    echo 'setup LCG_103, ROOT 6.28/00'
    lsetup "views LCG_103 $gcc"
    if [[ $2 == 'compile' ]]; then
        quickstats compile
    fi
elif [[ $lcg == '105' ]]; then
    echo 'setup LCG_105, ROOT 6.28/00'
    lsetup "views LCG_105 $gcc"
    if [[ $2 == 'compile' ]]; then
        quickstats compile
    fi
elif [[ $lcg == 'Stat' ]]; then
    echo 'setup StatAnalysis'
    asetup StatAnalysis,0.2.0
    return
else
    return
fi
