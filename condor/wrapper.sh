#!/usr/bin/env bash

echo
echo $@
echo
uname -a

export basedir=/afs/cern.ch/user/z/zhangr/work/HHcomb/DarkPhotonCombination/
cd $basedir
source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc11-opt/setup.sh
export PATH=${basedir}/bin:${basedir}/scripts:${basedir}/submodules/quickstats/bin:$PATH
export PYTHONPATH=${basedir}/bin:${basedir}:${basedir}/submodules/quickstats:$PYTHONPATH
echo PYTHONPATH=$PYTHONPATH,  MANPATH=$MANPATH,  PATH=$PATH

input=$1
mass=$2
dataset=$3
output=$4
figure=$5

input_ws=${input}/${mass}.root

POI=mu_SIG
output_dir=${output}/${dataset}_${mass}
figure_dir=${figure}/${dataset}_${mass}
mkdir -p $output_dir $figure_dir

echo "Input $input_ws"
echo "Output $output_dir"
echo "Figure $figure_dir"

option="--parallel 8 --constrained_only --exclude 'gamma_*'"
if [[ "$dataset" == *"asimov"* ]]; then
    option="$option -s $dataset"
    echo "Run Asimov" $dataset $option
else
    echo "Run Obs" $dataset $option
fi

echo 111
which python3
which python
echo 222
`which quickstats` --help
echo 333
eval `which quickstats` --help
echo 444
echo quickstats run_pulls -i $input_ws -x $POI -d $dataset $option -o $output_dir
quickstats run_pulls -i $input_ws -x $POI -d $dataset $option -o $output_dir

echo quickstats plot_pulls -i $output_dir -p $POI -o $figure_dir
quickstats plot_pulls -i $output_dir -p $POI -o $figure_dir

sleep 36000
