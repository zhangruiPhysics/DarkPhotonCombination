import os
import json
from quickstats.plots import UpperLimit1DPlot, UpperLimit2DPlot
import pandas as pd
import matplotlib.pyplot as plt

channel_label_map = {
    'VBF': r"VBF channel",
    'ZH': r"ZH channel",
    'gH': r"ggF channel",
    'VBFchannel': r"VBF channel",
    'ZHchannel': r"ZH channel",
    'ggFchannel': r"ggF channel",
    'combined': r"Combined",
}
color_pallete = {
    '2sigma': 'yellow',
    '1sigma': '#76FF7B',
}

labels = {
    'expected': 'Expected limit (95% CL)',
    'observed': 'Observed limit (95% CL)'
}

def remove_list(data):
    for k, v in data.items():
        for p,v_ in v.items():
            data[k][p] = v_[0]
    return data

def get_limit_data(channels = 'VBFchannel,ZHchannel', scale_factor=1, output_dir='./', tag=''):
    sm_limit_df = {}
    data = {}
    channels = channels.split(',')
    for c in channels + ['combined']:
        combine_tag = f'{"_".join(channels)}-{tag}'
        limit_path = os.path.join(output_dir, 'limit', c, combine_tag if c == 'combined' else '', 'nominal/limits_mu_SIG.json')
        if not os.path.exists(limit_path):
            print(limit_path, 'not found')
            combine_tag = f'A-{"_".join(channels)}-{combine_tag}'
            limit_path = os.path.join(output_dir, 'limits', c, combine_tag if c == 'combined' else '', 'limits.json')
            if not os.path.exists(limit_path):
                print(limit_path, 'not found')
                continue
        data[c] = json.load(open(limit_path))
    data_dict = remove_list(data)
    sm_limit_df = pd.DataFrame(data_dict) * scale_factor
    return sm_limit_df


def plot_limit_SM(sm_limit_df, suffix, status='int', output_dir='plot/'):
    plotting_order = ['combined', 'VBFchannel', 'ZHchannel']
    configs = {
        'top_margin': 2.,
    }
    styles = {
        'axis':{
            'tick_bothsides': False,
            'major_length': 12,
            },
        'legend':{
            'loc': 'upper right',
            },
    }
    analysis_label_options = {
        'loc': (0.03, 0.95),
        'fontsize':30, 'energy': '13 TeV', 
        'lumi': '139 fb$^{-1}$',
        'dy': 0.01,
        'status': status,
        'extra_text': r'SM $\mathit{H\to\gamma\gamma_{\mathrm{d}}}$'
    }
    
    filtered_order = [col for col in plotting_order if col in sm_limit_df.columns]
    sm_limit_df = sm_limit_df[filtered_order]
    if sm_limit_df['VBFchannel']['obs'] > 1.85: # switch to paper results 
        sm_limit_df['VBFchannel']['obs'] = 1.80
        sm_limit_df['VBFchannel']['0'] = 1.70
        sm_limit_df['VBFchannel']['-1'] = 1.20
        sm_limit_df['VBFchannel']['1'] = 2.40
        print('Switch to VBF paper results')

    plotter = UpperLimit1DPlot(sm_limit_df, channel_label_map, line_below=["VBFchannel"], labels=labels, color_pallete = color_pallete,
                               analysis_label_options=analysis_label_options, styles=styles, config=configs)
    xlabel = r"95% CL upper limit on BR($\mathit{H\to\gamma\gamma_{\mathrm{d}}}$) [%]"
    ax = plotter.draw(logx=False, xlabel=xlabel, draw_observed=True, draw_stat=False, sig_fig=1, add_text=True)
    ax.set_xlim([0, 8])
    os.makedirs(os.path.join(output_dir, f'{status}/limit'), exist_ok=True)
    filename = os.path.join(output_dir, f'{status}/limit', f"limit_mu{suffix}.pdf")
    plt.savefig(filename, bbox_inches="tight")
    plt.show()
    print('Save to', filename)
    sm_limit_df.to_csv(filename.replace('.pdf', '.csv'))
    
    
def get_limit_scan(channels = 'VBFchannel,ZHchannel', scale_factor=1, tag='', output_dir=''):
    data = {}
    channels = channels.split(',')
    for c in channels + ['combined']:
        combine_tag = f'{"_".join(channels)}-{tag}'
        limit_path = os.path.join(output_dir, 'limit', c, combine_tag if c == 'combined' else '', 'nominal/limits_mu_SIG.json')
        if not os.path.exists(limit_path):
            print(limit_path, 'not found')
            combine_tag = f'A-{"_".join(channels)}-{tag}'
            limit_path = os.path.join(output_dir, 'limits', c, combine_tag if c == 'combined' else '', 'limits.json')
        data[c] = json.load(open(limit_path))

    for c in data:
        df = pd.DataFrame(data[c]).set_index('mX')
        data[c] = df

    return data

def plot_individual(channel, ax, plotter, limit_df, linewidth=2):
    from copy import deepcopy
    color_pallete = {
        'VBFchannel': 'b',
        'ggFchannel': "r",
        'combined': 'k',
    }
    marker_style = {
        'VBFchannel': 'v',
        'ggFchannel': "^",
        'combined': 'o',
    }

    handles_map = {}
    for subchannel in limit_df:
        data = limit_df[subchannel]
        indices = data.index.astype(float).values
        exp_limits = data['0'].values
        obs_limits = data['obs'].values
        if subchannel == channel:
            alpha = 0
        else:
            alpha = 1
        ax.semilogy(indices, exp_limits, color=color_pallete[subchannel], linestyle='-.', alpha=alpha, label=channel_label_map[subchannel], linewidth=linewidth)
        handle_channels = ax.semilogy(indices, obs_limits, color=color_pallete[subchannel], marker=marker_style[subchannel], linestyle='-', linewidth=linewidth, alpha=1, label=channel_label_map[subchannel])
        handles_map[subchannel] = handle_channels[0]

    def update_legend_handles(handles):
        import matplotlib
        legend_data = {}

        for key in handles:
            handle = handles[key]
            if isinstance(handle, matplotlib.container.Container):
                label = handle.get_label()
            elif isinstance(handle, (tuple, list)):
                label = handle[0].get_label()
            else:
                label = handle.get_label()
            if label and not label.startswith('_'):
                legend_data[key] = {
                    'handle': handle,
                    'label': label
                }
            else:
                raise RuntimeError(f"the handle {handle} does not have an associated label")
        return legend_data

    def get_legend_handles_labels(legend_data):
        handles = []
        labels = []

        for key in ['VBFchannel', 'ggFchannel', 'combined']:
            if key in legend_data:
                handle = legend_data[key]['handle']
                label = legend_data[key]['label']
                handles.append(handle)
                labels.append(label)
        return handles, labels

    legend_data = update_legend_handles(handles_map)
    handles_sec, labels_sec = get_legend_handles_labels(legend_data)

    ax.add_artist(ax.get_legend())
    sec_style = deepcopy(plotter.styles['legend'])
    sec_style['loc'] = (0.60, 0.53)
    ax.legend(handles_sec, labels_sec, **sec_style)
    
    
def plot_xsection_scan(limit_df, channel='combined', xmin=None, xmax=None, ymin=None, ymax=None, add_individual=True, status='int', output_dir='plot'):

    analysis_label_options = {
        'status': status,
        'loc': (0.1, 0.95),
        'energy': '13 TeV',
        'lumi': r'139 fb$^{-1}$',
        'fontsize': 30,
        'dy': 0.01,
        'extra_text': r'BSM $\mathit{H\to\gamma\gamma_{\mathrm{d}}}$'
    }
    styles = {
        'legend':{
            'loc': (0.58, 0.73),
            'fontsize': 17
        },
        'axis':{
            'tick_bothsides': True,
            'major_length': 12,
            'labelsize': 25,
        },
        'xlabel': {
            'fontsize': 30,
        },
        'ylabel': {
            'fontsize': 30,
        },
    }
    config_options = {
       'expected_plot_styles': {
            'marker': 'None',
            'linestyle': '--',
            'alpha': 1,
            'linewidth': 3
        },
        'observed_plot_styles': {
            'marker': 'o',
            'alpha': 1,
            'linewidth': 3
        },
    }
    
    poi_name = r'$\mathit{m_{H}}$ [GeV]'
    ylabel = r"$\mathrm{BR}\times \sigma_{ggF+VBF}$ [pb]"

    plotter = UpperLimit2DPlot(limit_df[channel], labels={'expected': 'Expected limit (95% CL)'},
#                                scale_factor=theory_xs_values, 
                               styles=styles,
                               analysis_label_options=analysis_label_options,
                              color_pallete = color_pallete,
                              config = config_options)
    xmin = limit_df[channel].index.min() if not xmin else xmin
    xmax = limit_df[channel].index.max() if not xmax else xmax
    ylim = [7e-4, 0.1]
    ax = plotter.draw(xlabel=poi_name, ylabel=ylabel,
                 draw_observed=True, logy=True, ylim=ylim, xlim=[xmin, xmax])


    # Add individual curves on top
    if channel == 'combined' and add_individual:
        plot_individual(channel, ax, plotter, limit_df)

    os.makedirs(os.path.join(output_dir, f'{status}/limit'), exist_ok=True)
    suffix = ''
    if add_individual:
        suffix += '_withindiv'
    filename = os.path.join(output_dir, f'{status}/limit', f"limit_BSM{suffix}_{status}.pdf")
    plt.savefig(filename, bbox_inches="tight")
    print("save to", filename)
    for channel in limit_df:
        limit_df[channel].to_csv(filename.replace('.pdf', f'_{channel}.csv'))
    

if __name__ == '__main__':
    from argparse import ArgumentParser

    """Get arguments from command line."""
    parser = ArgumentParser(description='\033[92mCollect limits for different kappa-lambda values and plot.\033[0m')

    parser.add_argument('--input_folder', type=str, default='/output/v2', help='Input of the limit results (example: %(default)s).')
    parser.add_argument('--combine_tags', nargs='+', type=str, default='', help='Combined folder name (example: %(default)s).')
    parser.add_argument('--task', type=str, default='', choices=['SM', 'BSM'], help='Task (example: %(default)s).')
    parser.add_argument('--channel', type=str, default='', help='Channels as string separated by comma (example: %(default)s).')
    parser.add_argument('--status', type=str, default='int', choices=['int', 'prelim', 'final'], help='Task (example: %(default)s).')
    args = parser.parse_args()

    output_dir = os.path.join(os.environ['basedir'], args.input_folder)
    plotdir = output_dir+'/figures/'
    status = args.status
    channels = args.channel

    if args.task == 'SM':
        for combine_tag in args.combine_tags:
            suffix = ''
            if 'nocorr' in combine_tag:
                suffix = '_nocorr'
            sm_limit_df = get_limit_data(channels = channels, scale_factor=100, output_dir=output_dir, tag=combine_tag)
            plot_limit_SM(sm_limit_df,suffix,status, plotdir)

    else:
        for combine_tag in args.combine_tags:
            suffix = ''
            if 'nocorr' in combine_tag:
                suffix = '_nocorr'
            df = get_limit_scan(channels = channels, scale_factor=1, output_dir=output_dir, tag=combine_tag)
            plot_xsection_scan(df, status=status, output_dir=plotdir)
